import React, {useEffect} from "react";
import {Route, Redirect, useHistory} from "react-router-dom";
import {isLoaded, isEmpty} from "react-redux-firebase";
import {useSelector} from "react-redux";

export const PrivateRoute = ({children, ...remainingProps}) => {
	const auth = useSelector(state => state.firebase.auth);
	const profile = useSelector(state => state.firebase.profile);
	const history = useHistory();

	useEffect(() => {
		if (isLoaded(profile)
			&& profile.firstName == null
			&& profile.lastName == null) {
			history.push('/profile-setup')
		}
	}, [profile, history])

	return (
		<Route
			{...remainingProps}
			render={({location}) =>
				isLoaded(auth) && !isEmpty(auth) ? (children) :
					(<Redirect to={{
							pathname: "/sign-in",
							state: {from: location},
						}}/>
					)
			}
		/>
	);
};
