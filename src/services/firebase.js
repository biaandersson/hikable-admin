import * as firebase from "firebase/app";
import 'firebase/storage';
import "firebase/auth";
import "firebase/functions";
import "firebase/firestore";
import {store} from '../store/store'
import {createFirestoreInstance} from "redux-firestore";

const firebaseConfig = {
	apiKey: "AIzaSyAqzR5CzCM8dPGS0dNHIuNIevT4C5bUGXM",
	authDomain: "wolfpack-7a784.firebaseapp.com",
	databaseURL: "https://wolfpack-7a784.firebaseio.com",
	projectId: "wolfpack-7a784",
	storageBucket: "wolfpack-7a784.appspot.com",
	messagingSenderId: "248456425536",
	appId: "1:248456425536:web:e7f0b860eb5c15695a2fa5",
	measurementId: "G-GXWHSTHYN7"
};

firebase.initializeApp(firebaseConfig);
firebase.firestore();

if (window.location.hostname === 'localhost') {
	console.log("testing locally -- hitting local functions and firestore emulators");
	firebase.functions().useFunctionsEmulator('http://localhost:5001');
	firebase.firestore().settings({
		host: 'localhost:8080',
		ssl: false
	});
}

export default firebase;

export const db = firebase.firestore();

export const rrfConfig = {
	userProfile: "users",
	useFirestoreForProfile: true,
};

export const rrfProps = {
	firebase,
	config: rrfConfig,
	dispatch: store.dispatch,
	createFirestoreInstance
};
