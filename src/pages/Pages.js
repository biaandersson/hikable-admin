import React from 'react';
import {Create, SimpleForm, TextInput, DateInput} from 'react-admin';
import RichTextInput from "ra-input-rich-text";

export const CreatePages = props => (
	<Create {...props}>
		<SimpleForm>
			<TextInput source="title"/>
			<TextInput source="teaser" options={{multiLine: true}}/>
			<RichTextInput source="body"/>
			<DateInput label="Publication date" source="published_at"
			           defaultValue={new Date()}/>
		</SimpleForm>

	</Create>
);
