import React from 'react';
import {
	Edit,
	SimpleForm,
	TextInput,
	DateInput,
	DateField,
	EditButton,
	ReferenceInput,
	SelectInput,
	ImageInput,
	ImageField
} from 'react-admin';

import RichTextInput from "ra-input-rich-text";
import {required} from "ra-core";


export const PageEdit = (props) => {
	return (
		<Edit {...props}>
			<SimpleForm>
				<TextInput disabled label="Id" source="id"/>
				<TextInput source="title" validate={required()}/>
				<ReferenceInput label="Categories" source="categoriesName"
				                reference="categories">
					<SelectInput key="id" optionText="name" optionValue="name"/>
				</ReferenceInput>
				<TextInput multiline source="teaser" validate={required()}/>
				<DateInput label="Publication date" source="published_at"/>
				<RichTextInput source="body"/>
				<DateField source="created_at"/>
				<ImageInput source="pictures" label="Related pictures"
				            accept="image/*">
					<ImageField source="src" title="title"/>
				</ImageInput>
				<EditButton/>
			</SimpleForm>
		</Edit>
	)
};