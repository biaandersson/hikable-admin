import React from 'react';
import {Create, SimpleForm, TextInput} from 'react-admin';

export const CreateUser = props => (
	<Create {...props}>
		<SimpleForm>
			<TextInput source="firstName"/>
			<TextInput source="lastName"/>
			<TextInput source="email"/>
			<TextInput source="displayName"/>
		</SimpleForm>
	</Create>
);