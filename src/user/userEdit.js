import React from 'react';
import {
	Edit,
	SimpleForm,
	TextInput
} from 'react-admin';

export const UserEdit = props => (
	<Edit {...props}>
		<SimpleForm>
			<TextInput source="firstName"/>
			<TextInput source="lastName"/>
			<TextInput source="email"/>
			<TextInput source="displayName"/>
		</SimpleForm>
	</Edit>
);