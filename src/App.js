import * as React from 'react';
import {Admin, Resource} from 'react-admin';

import {
	FirebaseAuthProvider,
	FirebaseDataProvider,
} from 'react-admin-firebase';

import {UserList} from "./user/users";
import {UserEdit} from "./user/userEdit";
import {CreateUser} from "./user/createUser";
import LoginPage from "./login/LoginPage";
import {CreateCategory} from "./categories/Categories";
import {CategoryList} from "./categories/categoryList";
import {CategoryEdit} from "./categories/categoryEdit";
import {CreatePages} from "./pages/Pages";
import {PagesList} from "./pages/pagesList";
import {PageEdit} from "./pages/pageEdit";

const config = {
	apiKey: "AIzaSyAqzR5CzCM8dPGS0dNHIuNIevT4C5bUGXM",
	authDomain: "wolfpack-7a784.firebaseapp.com",
	databaseURL: "https://wolfpack-7a784.firebaseio.com",
	projectId: "wolfpack-7a784",
	storageBucket: "wolfpack-7a784.appspot.com",
	messagingSenderId: "248456425536",
	appId: "1:248456425536:web:e7f0b860eb5c15695a2fa5",
	measurementId: "G-GXWHSTHYN7"
};

const options = {};

const dataProvider = FirebaseDataProvider(config, options);
const authProvider = FirebaseAuthProvider(config, options);

export default function App() {
	return (
		<Admin dataProvider={dataProvider}
		       authProvider={authProvider}
		       loginPage={LoginPage}>
			<Resource
				name="users"
				list={UserList}
				edit={UserEdit}
				create={CreateUser}/>
			<Resource
				name="categories"
				list={CategoryList}
				create={CreateCategory}
				edit={CategoryEdit}/>
			<Resource
				name="pages"
				list={PagesList}
				create={CreatePages}
				edit={PageEdit}/>
		</Admin>
	);
}

