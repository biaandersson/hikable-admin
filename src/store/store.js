import {createBrowserHistory} from 'history'
import {routerMiddleware} from 'connected-react-router'
import {createStore, compose, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './reducers';
import {persistStore, persistReducer} from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import hardSet from 'redux-persist/lib/stateReconciler/hardSet'


export const history = createBrowserHistory()
const middleware = routerMiddleware(history)

const persistConfig = {
	key: 'root',
	storage,
	stateReconciler: hardSet,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = createStore(
	compose(
		applyMiddleware(
			thunk,
			middleware
		),
		window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
	)
)

export const persistor = persistStore(store)
